# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

#####################################################
## [2.3.0] - 2015-11-09
### Fixed
- lots of small fixes here and there

### Bugs
- None at known at this time

### Added
- Updated to ANTLR 4.4 and StringTemplate V4
- lots of logging
- Mohawk files can now have variable space and comments (pretty much the same as the Mohawk+T format except 
	with different literals and a strict ordering)
- CHANGELOG and VERSION files
- lots of small changes that got lost in the commit messages