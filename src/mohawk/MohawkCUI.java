package mohawk;

import java.io.*;
import java.util.*;

import org.apache.commons.cli.Options;

public class MohawkCUI {
    public static String previousCommandFilename = "MohawkCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        MohawkInstance inst = new MohawkInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!exit' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            cmd = user_input.next();
            fullCommand.append(cmd + " ");

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!exit")) {
                break;
            }

            if (cmd.equals("!prev")) {
                argv.clear();
                argv.addAll(Arrays.asList(previousCmd.split(" ")));
                break;
            }
            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        try {
            if (!cmd.equals("!prev")) {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            }
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
        }

        inst.run(argv.toArray(new String[1]));
    }

    public static void printCommonCommands() {
        System.out.println("\n\n--- Common Commands ---");
        System.out.println("-noheader -mode bmc -run all -input data/regressiontests/positive1.spec !exit");
        System.out.println("-noheader -mode bmc -run all -input data/regressiontests/positive2.spec !exit");
        System.out.println("-noheader -mode bmc -run all -input data/regressiontests/positive3.spec !exit");
        System.out.println("-run smv -input data/testcases/positive/test07.mohawk -smvfile positive7.smv !exit");
        System.out.println("-mode bmc -run all -input data/testcases/positive/test6.spec !exit");
        System.out.println("-mode bmc -run all -input data/testcases/positive/slice6.spec !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/positive !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/mixednocr !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/mixed !exit");
        System.out.println("-mode bmc -run all -bulk -input data/reductions !exit");
        System.out.println("");
        System.out.println("-mode bmc -run all -bulk -input data/tests !exit");
        System.out.println("-mode bmc -run smv -bulk -input data/tests !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/positive !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/mixednocr !exit");
        System.out.println("-mode bmc -run all -bulk -input data/testcases/mixed !exit");
        System.out.println("");
        System.out.println("-mode bmc -run all -input data/ahn/test.st.mohawk.T.mohawk !exit");
        System.out.println("-mode bmc -run all -input data/bug/isolation/ -skiprefine -bulk !exit");
        System.out.println("-mode bmc -run all -input data/TestsuiteC/hos -skiprefine -bulk !exit");
        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!prev' to use the previous command): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
