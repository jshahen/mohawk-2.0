package mohawk;

public enum OptionString {
    /** Displays the help menu and then exits*/
    HELP("help"),
    /** Displays the authors and then exits*/
    AUTHORS("authors"),
    /** Displays the version and then exits */
    VERSION("version"),
    /** Checks if NuSMV is on the path (or on the supplied path) and then exits*/
    CHECKNUSMV("checknusmv"),
    /** Provide a path to where NuSMV can be found */
    NUSMVPATH("nusmv"),
    /** Changes the log level. Available options are:
     * <ul>
     * <li> quiet   -- only severe errors are printed (results are printed out to the results files)
     * <li> warning -- severe and warning errors are printed
     * <li> info    -- DEFAULT; information relating to time, the results, and errors/warnings.
     * <li> fine    -- Gives a lot of extra information.
     * <li> debug   -- Gives the finest level of detail. Lots of debugging information.
     * </ul> */
    LOGLEVEL("loglevel"),
    /** The filename of the log file to use */
    LOGFILE("logfile"),
    /** The path to the log output folder; default is './logs/' */
    LOGFOLDER("logfolder"),
    /** Do not print the header for the CSV log file */
    NOHEADER("noheader"),
    /** Stores the results of each test that is run. */
    RESULTSFILE("output"),
    /** The maximum width of the console string allowed to be printed. Default line length is 180. */
    MAXW("maxw"),
    /** The new line string to use between log messages. Default is "\n    ". */
    LINESTR("linstr"),
    /** The input ARBAC-Safety policy file, or a folder containing those policy files. */
    SPECFILE("input"),
    /** The output SMV file that NuSMV file will use to solve the ARBAC-safety problem.
     * Default is "latestRBAC2SMV.smv" */
    SMVFILE("smvfile"),
    /**  */
    SPECEXT("specext"),
    /**  */
    BULK("bulk"),
    /**  */
    NOSLICING("noslicing"),
    /**  */
    SLICEQUERY("slicequery"),
    /**  */
    MODE("mode"),
    /**  */
    TIMEOUT("timeout"),
    /**  */
    RUN("run"),
    /** The name to attach to the results. Easy way to identify the purpose of a test. */
    RESULT_NAME("name"),
    /**  */
    SKIPREFINE("skiprefine");

    private String _str;

    private OptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }
}
