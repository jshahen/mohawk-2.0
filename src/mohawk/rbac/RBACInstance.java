/**
 * 			RBAC2SMV - Tool for converting a RBAC specification
 *                     to NuSMV specification
 *
 *			@author Karthick Jayaraman
 */
package mohawk.rbac;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.CREntry;

/** This class represents an RBAC instance read from an input file. An object of this class is passed to WriteNuSMV to
 * convert it to a NuSMV spec. */
public class RBACInstance {
    public final static Logger logger = Logger.getLogger("mohawk");

    private Vector<String> vRoles;
    private Vector<String> vUsers;
    private Vector<String> vAdmin;
    // integer 1,2,3,...,vRoles.size() -> the role name
    private Map<Integer, String> mRoleIndex;
    // integer 1,2,3,...,vUsers.size() -> the user name
    private Map<Integer, String> mUserIndex;

    // user name -> list of roles using their index
    private Map<String, Vector<Integer>> vUA;
    // target role name -> list of rules
    private Map<String, Vector<CREntry>> mCR;
    // target role name -> list of rules
    private Map<String, Vector<CAEntry>> mCA;
    // [user name, goal role name]
    private Vector<String> vSpec;

    public RBACInstance(Vector<String> invRoles, Vector<String> invUsers, Vector<String> invAdmin,
            Map<String, Vector<Integer>> invUA, Map<String, Vector<CREntry>> inmCR, Map<String, Vector<CAEntry>> inmCA,
            Vector<String> invSpec) {
        vRoles = invRoles;
        vUsers = invUsers;
        vAdmin = invAdmin;
        mRoleIndex = new HashMap<Integer, String>();
        mUserIndex = new HashMap<Integer, String>();
        vUA = invUA;
        mCR = inmCR;
        mCA = inmCA;
        vSpec = invSpec;

        // Setting up indices for users and roles
        for (int i = 0; i < vRoles.size(); i++) {
            mRoleIndex.put(i, vRoles.get(i));
        }

        for (int i = 0; i < vUsers.size(); i++) {
            mUserIndex.put(i, vUsers.get(i));
        }

    }

    public int getNumRoles() {
        return vRoles.size();
    }

    public int getNumRules() {
        return mCR.size() + mCA.size();
    }

    public int getUserIndex(String inStrUser) throws Exception {
        return getMapKey(mUserIndex, inStrUser);
    }

    public int getRoleIndex(String inStrRole) throws Exception {
        return getMapKey(mRoleIndex, inStrRole);
    }

    public String getUserFromIndex(Integer userIndex) throws Exception {
        return mUserIndex.get(userIndex);
    }

    public String getRoleFromIndex(Integer roleIndex) throws Exception {
        return mRoleIndex.get(roleIndex);
    }

    public Map<Integer, String> getRoleMap() {
        return mRoleIndex;
    }

    private int getMapKey(Map<Integer, String> inMap, String inString) throws Exception {

        for (int i = 0; i < inMap.size(); i++) {

            if (inMap.get(i).equals(inString)) { return i; }
        }

        throw new Exception("Error - BTree::getMapIndex - Value " + inString + " not found in map");
    }

    public Vector<String> getRoles() {
        return vRoles;
    }

    public Set<String> getRolesSet() {
        return new HashSet<String>(vRoles);
    }

    public Vector<String> getUsers() {
        return vUsers;
    }

    /** Get the admin users
     * 
     * @return */
    public Vector<String> getAdmin() {
        return vAdmin;
    }

    public Map<String, Vector<Integer>> getUA() {
        return vUA;
    }

    public Map<String, Vector<CREntry>> getCR() {
        return mCR;
    }

    public Map<String, Vector<CAEntry>> getCA() {
        return mCA;
    }

    public Vector<String> getSpec() {
        return vSpec;
    }

    public String getSpecUser() {
        return vSpec.get(0);
    }

    public String getSpecRole() {
        return vSpec.get(1);
    }

    public Vector<String> getAdminRoles() {
        return new Vector<String>(getAdminRolesSet());
    }

    public Set<String> getAdminRolesSet() {
        Set<String> roles = new HashSet<String>();

        for (Vector<CAEntry> cas : mCA.values()) {
            for (CAEntry ca : cas) {
                roles.add(ca.getAdminRole());
            }
        }

        return roles;
    }

    public Vector<String> getTargetRoles() {
        return new Vector<String>(getTargetRolesSet());
    }

    /** Returns the role names of the target CA rules
     * 
     * @return the keyset for mCA */
    public Set<String> getTargetRolesSet() {
        return mCA.keySet();
    }
}
