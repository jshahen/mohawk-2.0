/**
 * 			RBAC2SMV - Tool for converting a RBAC specification
 *                     to NuSMV specification
 *
 *			@author Karthick Jayaraman
 */
package mohawk.collections;

import java.util.Vector;

/*
 *  This class abstracts a bit vector. Internally it contains a vector of Boolean. 
 *  Supports bitwise operations such as AND and OR. 
 *  May extend to additional operations in the future. 
 */

public class BitVector {

    private Vector<Boolean> vecBV;
    private int size;

    public BitVector(int inSize, Boolean inValue) {

        size = inSize;
        vecBV = new Vector<Boolean>(size);
        for (int i = 0; i < size; i++) {
            vecBV.addElement(inValue);
        }

    }

    public int size() {
        return size;
    }

    public void printBV() {

        int size = vecBV.size();

        System.out.println("Size of the bit vector: " + size);
        for (int i = 0; i < size; i++) {

            System.out.print(vecBV.get(i).toString() + " ");

        }
        System.out.println();
    }

    // Get the value at a specified index
    public boolean atIndex(int inIndex) {

        return vecBV.get(inIndex);
    }

    // Set the value at a specified index
    public void putAt(int inIndex, boolean inValue) {

        vecBV.set(inIndex, inValue);
    }

    // logical AND operation
    public BitVector andBV(BitVector inOperand2) throws Exception {

        if (size != inOperand2.size) { throw new Exception("Error - BitVector::andBV - Operand sizes do not match"); }

        BitVector result = new BitVector(size, false);

        for (int i = 0; i < size; i++) {
            result.putAt(i, atIndex(i) & inOperand2.atIndex(i));
        }

        return result;
    }

    // logical OR operation
    public BitVector orBV(BitVector inOperand2) throws Exception {

        if (size != inOperand2.size) { throw new Exception("Error - BitVector::orBV - Operand sizes do not match"); }

        BitVector result = new BitVector(size, false);

        for (int i = 0; i < size; i++) {
            result.putAt(i, atIndex(i) | inOperand2.atIndex(i));
        }

        return result;
    }

}
