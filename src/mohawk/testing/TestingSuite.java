package mohawk.testing;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.ExecutionResult;
import mohawk.global.results.TestingResult;
import mohawk.math.CalculateDiameter;
import mohawk.output.WriteNuSMV;
import mohawk.rbac.RBACInstance;
import mohawk.rbac.RBACSpecReader;
import mohawk.refine.RemoveUnsetRoles;
import mohawk.refine.RolesAbsRefine;
import mohawk.singleton.MohawkSettings;
import mohawk.slicer.RoleSlicer;
import mohawk.slicer.SliceQueryRole;

public class TestingSuite {
    public final static Logger logger = Logger.getLogger("mohawk");
    private MohawkSettings settings = MohawkSettings.getInstance();

    public TestingSuite() {
        settings.timing.startTimer("totalTime");
    }

    public Boolean done() {
        settings.timing.stopTimer("totalTime");

        boolean error = false;

        File timingResults = new File(settings.timingResultsFile);
        try {
            settings.timing.writeOut(timingResults);
        } catch (IOException e) {
            logger.warning(
                    "[TIMING] Unable to save the Timing Results to the file: " + timingResults.getAbsolutePath());
            error = true;
        }
        logger.info("[TIMING] Saved Timing Results to the file: " + timingResults.getAbsolutePath());

        return !error;
    }

    /** This will take care of fully running the test, if it needs to convert SPEC files it will and while it runs it
     * will write out results after each SMV file is run (just in case of a crash).
     * 
     * @throws IOException */
    public void runTests() throws IOException {
        logger.entering(getClass().getName(), "runTests()");

        logger.info("[runTests] Results File: " + settings.testingResultsFile);
        FileWriter resultsFW = settings.results.getFileWriter(new File(settings.testingResultsFile), false);

        if (resultsFW == null) {
            logger.severe("[runTests] Unable to Create, or Write in, the results file: " + settings.testingResultsFile);
            throw new IOException("Unable to Create, or Write in, the results file: " + settings.testingResultsFile);
        }

        ArrayList<NuSMVMode> modes = new ArrayList<NuSMVMode>();
        switch (settings.mode) {
            case 3 :// Both
                modes.add(NuSMVMode.SMC);
                modes.add(NuSMVMode.BMC);
                break;
            case 1 :// BMC
                modes.add(NuSMVMode.BMC);
                break;
            case 2 :// SMC
                modes.add(NuSMVMode.SMC);
                break;
            default :
                logger.severe("[ERROR] Unknown mode = " + new Integer(settings.mode));
        }

        /* TIMING */settings.timing.startTimer("smvHelper.loadSpecFiles");
        try {
            settings.smvHelper.loadSpecFiles();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        /* TIMING */settings.timing.stopTimer("smvHelper.loadSpecFiles");

        /* TIMING */settings.timing.startTimer("runTests.mainLoop");
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Integer numSpecFiles = settings.smvHelper.specFiles.size();
        for (Integer i = 0; i < numSpecFiles; i++) {
            /* TIMING */settings.timing.startTimer("runTests.mainLoop (" + (i + 1) + "/" + numSpecFiles + ")");
            for (Integer j = 0; j < modes.size(); j++) {
                try {
                    String specFile = settings.smvHelper.specFiles.get(i).getAbsolutePath();
                    String timerName = "runTests.mainLoop (" + (i + 1) + "/" + numSpecFiles + ") - "
                            + settings.smvHelper.specFiles.get(i).getName() + " (" + modes.get(j) + ")";

                    String comment = "Mode: " + modes.get(j) + "; " + settings.toString();

                    /* TIMING */settings.timing.startTimer(timerName);
                    logger.info("Working on " + timerName);
                    System.out.println("---\n" + "Working on " + timerName);

                    RBACSpecReader reader = new RBACSpecReader(specFile);
                    RBACInstance rbac = reader.getRBAC();

                    logger.info("[DETAILS] Number of Roles: " + rbac.getNumRoles() + "; Number of Rules: "
                            + rbac.getNumRules());

                    // *******************************************************************************
                    // Heuristic #1
                    if (settings.runHeuristics == true) {
                        if (!rbac.getTargetRolesSet().contains(rbac.getSpecRole())) {
                            System.out.println("[HEURISTIC #1] Cannot find SPEC role in any of the CA target roles! "
                                    + "This RBAC Instance is unreachable!");

                            // Setting up the lastResult information
                            /* TIMING */settings.timing.stopTimer(timerName);
                            settings.lastResult = new TestingResult(ExecutionResult.GOAL_UNREACHABLE,//
                                    ExecutionResult.GOAL_UNKNOWN,//
                                    settings.timing.getLastElapsedTime(),//
                                    settings.resultsName, //
                                    settings.smvHelper.specFiles.get(i).getAbsolutePath(), //
                                    "Mohawk+NuSMV [Heuristics]",// Program
                                    modes.get(j).toString(), //
                                    null, // Bound
                                    "0", //
                                    comment + "; Result obtained by Heuristic #1");

                            System.out.println(
                                    "Result: " + settings.lastResult + " for role '" + rbac.getSpecRole() + "'");
                            logger.info("[COMPLETED] Result: " + settings.lastResult + ", for the Spec Role: '"
                                    + rbac.getSpecRole() + "' in following spec file (in mode " + modes.get(j) + "): "
                                    + specFile);

                            settings.results.add(settings.lastResult);
                            settings.results.writeOutLast(resultsFW);

                            continue;
                        }
                    }
                    // *******************************************************************************

                    try {
                        rbac = this.optimizeRBAC(rbac, timerName);
                    } catch (Exception e) {
                        StringWriter errors = new StringWriter();
                        e.printStackTrace(new PrintWriter(errors));
                        logger.severe(errors.toString());
                        logger.severe(e.getMessage());

                        continue;
                    }
                    logger.info("[DETAILS] (After Optimizing) Number of Roles: " + rbac.getNumRoles()
                            + "; Number of Rules: " + rbac.getNumRules());

                    if (rbac.getNumRoles() == 0) {
                        logger.info("[HEURISTIC #3a] There are no roles after optimization, thus the state is "
                                + "UNREACHABLE and will not convert to SMV and run");
                        // Setting up the lastResult information
                        /* TIMING */settings.timing.stopTimer(timerName);
                        settings.lastResult = new TestingResult(ExecutionResult.GOAL_UNREACHABLE,//
                                ExecutionResult.GOAL_UNKNOWN,//
                                settings.timing.getLastElapsedTime(),//
                                settings.resultsName, //
                                settings.smvHelper.specFiles.get(i).getAbsolutePath(), //
                                "Mohawk+NuSMV [Heuristics]",// Program
                                modes.get(j).toString(), //
                                null, // Bound
                                "0", //
                                comment + "; Result obtained by Heuristic #3a");

                        System.out.println("Result: " + settings.lastResult + " for role '" + rbac.getSpecRole() + "'");
                        logger.info("[COMPLETED] Result: " + settings.lastResult + ", for the Spec Role: '"
                                + rbac.getSpecRole() + "' in following spec file (in mode " + modes.get(j) + "): "
                                + specFile);

                        settings.results.add(settings.lastResult);
                        settings.results.writeOutLast(resultsFW);
                        continue;
                    }

                    if (rbac.getNumRules() == 0) {
                        logger.info(
                                "[HEURISTIC #3b] There are no rules after optimization, thus the state is UNREACHABLE "
                                        + "and will not convert to SMV and run");
                        // Setting up the lastResult information
                        /* TIMING */settings.timing.stopTimer(timerName);
                        settings.lastResult = new TestingResult(ExecutionResult.GOAL_UNREACHABLE,//
                                ExecutionResult.GOAL_UNKNOWN,//
                                settings.timing.getLastElapsedTime(),//
                                settings.resultsName, //
                                settings.smvHelper.specFiles.get(i).getAbsolutePath(), //
                                "Mohawk+NuSMV [Heuristics]",// Program
                                modes.get(j).toString(), //
                                null, // Bound
                                "0",//
                                comment + "; Result obtained by Heuristic #3b");

                        System.out.println("Result: " + settings.lastResult + " for role '" + rbac.getSpecRole() + "'");
                        logger.info("[COMPLETED] Result: " + settings.lastResult + ", for the Spec Role: '"
                                + rbac.getSpecRole() + "' in following spec file (in mode " + modes.get(j) + "): "
                                + specFile);

                        settings.results.add(settings.lastResult);
                        settings.results.writeOutLast(resultsFW);
                        continue;
                    }

                    // *******************************************************************************
                    // Heuristic #2
                    if (settings.runHeuristics == true) {
                        boolean ruleIsStartable = false;

                        for (CAEntry carule : rbac.getCA().get(rbac.getSpecRole())) {
                            if (carule.isStartable()) {
                                ruleIsStartable = true;
                                break;
                            }
                        }

                        if (ruleIsStartable) {
                            System.out.println("[HEURISTIC #2] Found a startable CA rule with the SPEC role, "
                                    + "thus this rule can fire upon starting and thus solve the SPEC in 1 step.");

                            // Setting up the lastResult information
                            /* TIMING */settings.timing.stopTimer(timerName);
                            settings.lastResult = new TestingResult(ExecutionResult.GOAL_REACHABLE,//
                                    ExecutionResult.GOAL_UNKNOWN,//
                                    settings.timing.getLastElapsedTime(),//
                                    settings.resultsName, //
                                    settings.smvHelper.specFiles.get(i).getAbsolutePath(), //
                                    "Mohawk+NuSMV [Heuristics]",// Program
                                    modes.get(j).toString(), //
                                    null, // Bound
                                    "0", //
                                    comment + "; Result obtained by Heuristic #2");

                            System.out.println(
                                    "Result: " + settings.lastResult + " for role '" + rbac.getSpecRole() + "'");
                            logger.info("[COMPLETED] Result: " + settings.lastResult + ", for the Spec Role: '"
                                    + rbac.getSpecRole() + "' in following spec file (in mode " + modes.get(j) + "): "
                                    + specFile);

                            settings.results.add(settings.lastResult);
                            settings.results.writeOutLast(resultsFW);

                            continue;
                        }
                    }
                    // *******************************************************************************

                    RolesAbsRefine absrefine = new RolesAbsRefine(rbac, specFile, settings.timing, timerName);
                    absrefine.setMode(modes.get(j));

                    // Setup Timeout Timer
                    Future<TestingResult> future = executor.submit(new TestRunner(absrefine));
                    try {
                        logger.info("[RUNNING] Running Mohawk on testcase " + (i + 1) + "/" + numSpecFiles + " (mode="
                                + modes.get(j) + "):");

                        /* TIMING */settings.timing.startTimer(timerName + " (TestRunner)");
                        if (settings.TIMEOUT_SECONDS == 0) {
                            logger.info("[OPTION] Disabling the TIMEOUT feature");
                            settings.lastResult = future.get();
                        } else {
                            logger.info("[OPTION] Setting TIMEOUT to " + settings.TIMEOUT_SECONDS + " seconds");
                            settings.lastResult = future.get(settings.TIMEOUT_SECONDS, TimeUnit.SECONDS);
                        }
                        /* TIMING */settings.timing.stopTimer(timerName + " (TestRunner)");

                        // Setting up the lastResult information
                        /* TIMING */settings.timing.stopTimer(timerName);
                        settings.lastResult._duration = settings.timing.getLastElapsedTime();
                        settings.lastResult._filename = settings.smvHelper.specFiles.get(i).getAbsolutePath();
                        if (settings.lastResult._comment == null || settings.lastResult._comment.isEmpty()) {
                            settings.lastResult._comment = comment;
                        } else {
                            settings.lastResult._comment += "; " + comment;
                        }

                        System.out.println("Result: " + settings.lastResult + " for role '" + absrefine.getSpecRole() //
                                + "'");
                        logger.info("[COMPLETED] Result: " + settings.lastResult + ", for the Spec Role: '"
                                + absrefine.getSpecRole() + "' in following spec file (in mode " + modes.get(j) + "): "
                                + specFile);

                        settings.results.add(settings.lastResult);
                        settings.results.writeOutLast(resultsFW);
                    } catch (TimeoutException e) {
                        logger.warning("[TIMEOUT] Mohawk has timed out for SPEC file: " + specFile);
                        /* TIMING */settings.timing.cancelTimer(timerName);

                        settings.lastResult = new TestingResult(ExecutionResult.TIMEOUT,//
                                ExecutionResult.GOAL_UNKNOWN,//
                                settings.timing.getLastElapsedTime(), //
                                settings.resultsName, //
                                settings.smvHelper.specFiles.get(i), //
                                "Mohawk+NuSMV",// Program
                                modes.get(j).toString(), //
                                (settings.lastResult != null) ? settings.lastResult._bound : null, "timeout", //
                                comment);

                        settings.results.writeOutLast(resultsFW);
                    } catch (OutOfMemoryError e) {
                        logger.warning("[OUT OF MEMORY] Mohawk has ran out of memory out for SPEC file: " + specFile);
                        /* TIMING */settings.timing.cancelTimer(timerName);

                        settings.lastResult = new TestingResult(ExecutionResult.OUT_OF_MEMORY,//
                                ExecutionResult.GOAL_UNKNOWN,//
                                settings.timing.getLastElapsedTime(), //
                                settings.resultsName, //
                                settings.smvHelper.specFiles.get(i), //
                                "Mohawk+NuSMV",// Program
                                modes.get(j).toString(), //
                                (settings.lastResult != null) ? settings.lastResult._bound : null, "out_of_memory", //
                                comment);

                        settings.results.writeOutLast(resultsFW);
                    } catch (InterruptedException e) {
                        if (logger.getLevel() == Level.FINEST) {
                            e.printStackTrace();
                        }
                        /* TIMING */settings.timing.cancelTimer(timerName);
                        logger.severe(e.getMessage());
                    } catch (ExecutionException e) {
                        StringWriter errors = new StringWriter();
                        e.printStackTrace(new PrintWriter(errors));
                        logger.severe(errors.toString());
                        logger.severe(e.getMessage());
                        /* TIMING */settings.timing.cancelTimer(timerName);
                    }

                    logger.info("Previous Elapsed Time: " + settings.timing.getLastElapsedTimeSec() + " seconds");
                } catch (IOException e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    logger.severe(errors.toString());
                    logger.severe(e.getMessage());

                    continue;
                }
            }
            /* TIMING */settings.timing.stopTimer("runTests.mainLoop (" + (i + 1) + "/" + numSpecFiles + ")");
        }
        executor.shutdown();
        try {
            resultsFW.close();
        } catch (IOException e) {
            // DO NOTHING
        }
        /* TIMING */settings.timing.stopTimer("runTests.mainLoop");

        logger.exiting(getClass().getName(), "runTests()");
    }

    public RBACInstance optimizeRBAC(RBACInstance rbac, String timerName) throws Exception {
        int numRoles, numRules;
        if (settings.sliceRBAC) {
            logger.info("[Slicing] Slicing the Spec File");
            /* TIMING */settings.timing.startTimer(timerName + " (slicing)");

            numRoles = rbac.getNumRoles();
            numRules = rbac.getNumRules();

            RoleSlicer roleslicer = new RoleSlicer(rbac, settings.timing, timerName);
            rbac = roleslicer.getSlicedPolicy();

            logger.info("[DETAILS] After Slicing -> " + "Number of Roles: " + numRoles + "->" + rbac.getNumRoles()
                    + " (" + (numRoles - rbac.getNumRoles()) + " diff); " + "Number of Rules: " + numRules + "->"
                    + rbac.getNumRules() + " (" + (numRules - rbac.getNumRules()) + " diff); ");

            /* TIMING */settings.timing.stopTimer(timerName + " (slicing)");
        }

        if (settings.sliceRBACQuery) {
            logger.info("[Slicing] Slicing the Spec's Query");
            /* TIMING */settings.timing.startTimer(timerName + " (slicingQuery)");

            numRoles = rbac.getNumRoles();
            numRules = rbac.getNumRules();

            SliceQueryRole scRole = new SliceQueryRole(rbac);
            rbac = scRole.getSlicedPolicy();

            logger.info("[DETAILS] After Slicing Query -> " + "Number of Roles: " + numRoles + "->" + rbac.getNumRoles()
                    + " (" + (numRoles - rbac.getNumRoles()) + " diff); " + "Number of Rules: " + numRules + "->"
                    + rbac.getNumRules() + " (" + (numRules - rbac.getNumRules()) + " diff); ");

            /* TIMING */settings.timing.stopTimer(timerName + " (slicingQuery)");
        }

        if (settings.removeUnsetRolesRBAC) {
            logger.info("[Slicing] Optimizing the RBAC Instance file by removing the unset roles");
            /* TIMING */settings.timing.startTimer(timerName + " (removeUnsetRoles)");

            numRoles = rbac.getNumRoles();
            numRules = rbac.getNumRules();

            RemoveUnsetRoles ruRole = new RemoveUnsetRoles(rbac, settings.timing, timerName);
            rbac = ruRole.getOptimizedRBAC();

            logger.info("[DETAILS] After Removing Unused Roles -> " + "Number of Roles: " + numRoles + "->"
                    + rbac.getNumRoles() + " (" + (numRoles - rbac.getNumRoles()) + " diff); " + "Number of Rules: "
                    + numRules + "->" + rbac.getNumRules() + " (" + (numRules - rbac.getNumRules()) + " diff); ");

            /* TIMING */settings.timing.stopTimer(timerName + " (removeUnsetRoles)");
        }

        logger.info("[DETAILS] (before returning) Number of Roles: " + rbac.getNumRoles() + "; Number of Rules: "
                + rbac.getNumRules());

        return rbac;
    }

    /** If you only want to convert SPEC files to SMV files. This can be used before running if there happens to be a
     * crash while running the tests you can just resume from the test and not have to convert the SPEC to SMV every
     * time.
     * 
     * @throws IOException */
    public void onlyConvertSpecToSmvFormat() throws IOException {
        logger.entering(getClass().getName(), "onlyConvertSpecToSmvFormat()");

        settings.smvHelper.loadSpecFiles();

        for (int i = 0; i < settings.smvHelper.specFiles.size(); i++) {
            try {
                File specFile = settings.smvHelper.specFiles.get(i);
                File nusmvFile = settings.smvHelper.getSmvFile(specFile);
                String timerName = "onlyConvertSpecToSmvFormat.mainLoop (" + (i + 1) + "/"
                        + settings.smvHelper.specFiles.size() + ") - " + specFile.getName();

                logger.info("[FILE IO] Using SMV File: " + nusmvFile.getCanonicalPath());

                if (!specFile.exists()) {
                    logger.severe("[ERROR] Unable to find the SPEC file: " + specFile.getName());
                    throw new FileNotFoundException("Unable to find the SPEC file: " + specFile.getName());
                }

                WriteNuSMV nusmv = new WriteNuSMV(nusmvFile.getAbsolutePath(), settings.timing,
                        "convertSpecToSMVFormat(" + specFile.getName() + ")");
                RBACSpecReader reader = new RBACSpecReader(specFile.getAbsolutePath());
                RBACInstance rbac = reader.getRBAC();

                rbac = optimizeRBAC(rbac, timerName);

                logger.info("[DETAILS] (after optimizing 2)Number of Roles: " + rbac.getNumRoles()
                        + "; Number of Rules: " + rbac.getNumRules());

                if (rbac.getNumRoles() == 0) {
                    logger.info("[HEURISTIC #3a] There are no roles after optimization, thus the state isd UNREACHABLE "
                            + "and will not convert to SMV and run");
                    continue;
                }

                if (rbac.getNumRules() == 0) {
                    logger.info("[HEURISTIC #3b] There are no rules after optimization, thus the state isd UNREACHABLE "
                            + "and will not convert to SMV and run");
                    continue;
                }

                nusmv.fillAttributes(rbac);

                System.out.println("Number of role without rules: " + nusmv.roleWithoutRules);
                System.out.println("Number of role with rules: " + nusmv.roleWithRules);
                System.out.println("Ratio without / with: " + (new Float(nusmv.roleWithoutRules) / nusmv.roleWithRules)
                        + "; without / total: "
                        + (new Float(nusmv.roleWithoutRules) / (nusmv.roleWithRules + nusmv.roleWithoutRules)));

                nusmv.writeFile();

                // This option is available for testing purposes as there isn't a
                // practical reason to convert and immediately delete
                if (settings.smvDeleteFile) {
                    logger.fine("[FILE IO] Deleting SMV File: " + nusmvFile.getName());
                    nusmvFile.delete();
                }
            } catch (Exception e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                logger.severe(errors.toString());
                logger.severe(e.getMessage());
            }
        }

        logger.exiting(getClass().getName(), "onlyConvertSpecToSmvFormat()");
    }

    /** Displays the calculated Diameter of input RBAC Spec Files
     * 
     * @throws IOException */
    public void calculateDiameter() throws IOException {
        logger.entering(getClass().getName(), "calculateDiameter()");

        settings.smvHelper.loadSpecFiles();
        Map<String, Integer> diameter;
        for (int i = 0; i < settings.smvHelper.specFiles.size(); i++) {
            File specFile = settings.smvHelper.specFiles.get(i);

            diameter = CalculateDiameter.CalDiameterFile(specFile);
            if (diameter == null) {
                logger.log(Level.SEVERE, "[DIAMETER] Error calculating diameter for {1}", specFile.getAbsolutePath());
            } else {
                logger.log(Level.INFO, "[DIAMETER] Diameter: {0}, Roles: {1}, Tightenings: {2}, File: {3}",
                        new Object[]{diameter.get("Diameter"), diameter.get("NumRoles"), diameter.get("NumTightenings"),
                                specFile.getAbsolutePath()});
            }
        }

        logger.exiting(getClass().getName(), "calculateDiameter()");
    }
}
