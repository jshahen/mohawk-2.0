/**
 * 
 */
package mohawk.math;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.CAEntry;
import mohawk.global.pieces.mohawk.PreCondition;
import mohawk.rbac.RBACInstance;

/** @author kjayaram */
public class CalculateRplusOnly {
    public final static Logger logger = Logger.getLogger("mohawk");

    RBACInstance rbac_instance;
    Set<String> R_pos;
    Set<String> R_rpos;

    public CalculateRplusOnly(RBACInstance in_rbac) {
        rbac_instance = in_rbac;
        R_pos = new HashSet<String>();
        R_rpos = new HashSet<String>();
        CalculateRp();
        CalculateRrp();

        R_pos.removeAll(R_rpos);
    }

    public Set<String> getRplusOnly() {
        return R_pos;
    }

    public void CalculateRp() {

        LinkedList<String> qRoleQueue = new LinkedList<String>();
        qRoleQueue.add(rbac_instance.getSpec().get(1));

        while (qRoleQueue.size() > 0) {

            String nxtRole = qRoleQueue.remove();
            Vector<CAEntry> vCAE = rbac_instance.getCA().get(nxtRole);

            if (vCAE == null) continue;

            for (int i = 0; i < vCAE.size(); i++) {

                CAEntry nxtCAE = vCAE.get(i);
                PreCondition nxtPreCond = nxtCAE.getPreConditions();

                for (int roleindex : nxtPreCond.keySet()) {

                    if (nxtPreCond.getConditional(roleindex) == 1) {

                        String strPosRole = rbac_instance.getRoles().get(roleindex);

                        if (!R_pos.contains(strPosRole)) {
                            R_pos.add(strPosRole);
                            qRoleQueue.add(strPosRole);
                        }
                    }
                }
            }
        }
    }

    public void CalculateRrp() {

        LinkedList<String> RoleQueue = new LinkedList<String>();
        RoleQueue.addAll(R_pos);

        while (RoleQueue.size() > 0) {

            String strRole = RoleQueue.remove();
            Set<String> sNegRoles = getNegTargets(strRole);

            Iterator<String> itr = sNegRoles.iterator();
            while (itr.hasNext()) {
                String nxtRole = itr.next();
                if (!R_rpos.contains(nxtRole)) {
                    R_rpos.add(nxtRole);
                    RoleQueue.add(nxtRole);
                }
            }
        }
    }

    public Set<String> getNegTargets(String strRole) {

        int iRoleIndex = 0;
        try {
            iRoleIndex = rbac_instance.getRoleIndex(strRole);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Set<String> sNegRoles = new HashSet<String>();

        for (String strTgtRole : rbac_instance.getCA().keySet()) {

            Vector<CAEntry> vCAE = rbac_instance.getCA().get(strTgtRole);
            if (vCAE == null) continue;

            for (int i = 0; i < vCAE.size(); i++) {

                PreCondition precond = vCAE.get(i).getPreConditions();
                if (precond.keySet().contains(iRoleIndex)) {
                    if (precond.getConditional(iRoleIndex) == 2) sNegRoles.add(strRole);
                }
            }
        }

        return sNegRoles;
    }

}
