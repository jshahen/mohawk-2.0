/**
 * The MIT License
 * 
 * Copyright (c) 2010 Karthick Jayaraman
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package mohawk.refine;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.mohawk.*;
import mohawk.rbac.RBACInstance;

/** @author Karthick Jayaraman
 * 
 *         NOT USED */
public class FwdPruning {
    public final static Logger logger = Logger.getLogger("mohawk");

    RBACInstance unsliced;
    RBACInstance sliced;
    String strRole;
    String strUser;

    Vector<String> slicedRoles;
    Vector<String> slicedUsers;
    Vector<String> slicedAdmin;
    Map<String, Vector<Integer>> slicedUA;
    Map<String, Vector<CREntry>> slicedCR;
    Map<String, Vector<CAEntry>> slicedCA;
    Map<String, Vector<CAEntry>> unslicedCA;
    Vector<String> slicedSpec;

    Set<String> Roles_low;
    Set<String> Roles_up;

    public FwdPruning(RBACInstance inRbac) {

        unsliced = inRbac;
        strRole = unsliced.getSpec().get(1);
        strUser = unsliced.getSpec().get(0);
    }

    public Set<String> getRolesLow() {

        Set<String> Roles_low = new HashSet<String>();
        Vector<Integer> vecUA = unsliced.getUA().get(strUser);

        if (vecUA != null) {
            for (int i = 0; i < vecUA.size(); i++) {
                int roleindex = vecUA.get(i);
                String strRole = unsliced.getRoles().get(roleindex);

                if (!unsliced.getCR().containsKey(strRole)) Roles_low.add(strRole);
            }
        }

        return Roles_low;
    }

    // Gather the initial Roles_up from the UA
    public Set<String> getRolesUp() {

        Set<String> Roles_up = new HashSet<String>();
        Vector<Integer> vecUA = unsliced.getUA().get(strUser);

        if (vecUA != null) {
            for (int i = 0; i < vecUA.size(); i++) {
                int roleindex = vecUA.get(i);
                String strRole = unsliced.getRoles().get(roleindex);

                Roles_up.add(strRole);
            }
        }

        return Roles_up;
    }

    public Boolean canApply(Set<String> Roles_low, Set<String> Roles_up, PreCondition preconditions) {

        for (int roleindex : preconditions.keySet()) {
            String strRole = unsliced.getRoles().get(roleindex);

            if (preconditions.getConditional(roleindex) == 1) {
                if (!Roles_up.contains(strRole)) return false;
            } else {
                if (Roles_low.contains(strRole)) return false;
            }
        }

        return true;
    }

    private Boolean FwdPruneHelper() {

        // Map<String,Vector<CAEntry>> unslicedCA = unsliced.getCA();

        Boolean added = false;

        Iterator<String> it = unslicedCA.keySet().iterator();
        while (it.hasNext()) {
            String strTargetRole = it.next();
            Vector<CAEntry> vecCA = unslicedCA.get(strTargetRole);
            Iterator<CAEntry> itcae = vecCA.iterator();

            // for(int i=0; i<vecCA.size(); i++)
            while (itcae.hasNext()) {
                CAEntry caentry = itcae.next();// vecCA.get(i);

                if (this.canApply(Roles_low, Roles_up, caentry.getPreConditions())) {
                    Roles_up.add(strTargetRole);
                    Vector<CAEntry> vecCAE = slicedCA.get(strTargetRole);
                    if (vecCAE == null) vecCAE = new Vector<CAEntry>();
                    vecCAE.add(caentry);
                    slicedCA.put(strTargetRole, vecCAE);
                    added = true;

                    itcae.remove();
                }
            }
        }

        return added;
    }

    public void FwdPrune() {
        Roles_low = getRolesLow();
        Roles_up = getRolesUp();

        unslicedCA = unsliced.getCA();
        if (slicedCA == null) slicedCA = new HashMap<String, Vector<CAEntry>>();

        while (FwdPruneHelper())
            ;

        // Roles_up.add(unsliced.getRoles().lastElement());
        Roles_up.add(unsliced.getAdminRoles().firstElement());
    }

    // This function has to be called only after initializing slicedRoles
    public void sliceUA() {

        // Create mapping between sliced and unsliced roles.
        Map<Integer, Integer> mSliced2Unsliced = new HashMap<Integer, Integer>();
        Map<Integer, Integer> mUnsliced2Sliced = new HashMap<Integer, Integer>();

        for (int i = 0; i < slicedRoles.size(); i++) {
            mSliced2Unsliced.put(i, unsliced.getRoles().indexOf(slicedRoles.get(i)));
            mUnsliced2Sliced.put(unsliced.getRoles().indexOf(slicedRoles.get(i)), i);
        }

        slicedUA = new HashMap<String, Vector<Integer>>();

        for (String user : unsliced.getUA().keySet()) {
            Vector<Integer> unslicedUA = unsliced.getUA().get(user);
            Vector<Integer> slicedvUA = new Vector<Integer>();

            for (int i = 0; i < unslicedUA.size(); i++) {
                if (mUnsliced2Sliced.containsKey(unslicedUA.get(i)))
                    slicedvUA.add(mUnsliced2Sliced.get(unslicedUA.get(i)));
            }

            slicedUA.put(user, slicedvUA);
        }
    }

    public RBACInstance getSlicedPolicy() {

        slicedRoles = new Vector<String>();
        slicedUsers = new Vector<String>();

        this.FwdPrune(); // Forward pruning will produce Role_low, Roles_up, and
                         // slicedCA

        slicedRoles.addAll(Roles_low);
        slicedRoles.addAll(Roles_up);
        slicedUsers.addAll(unsliced.getAdmin());
        slicedUsers.add(strUser);

        slicedCR = unsliced.getCR();
        sliceUA();

        return new RBACInstance(slicedRoles, slicedUsers, unsliced.getAdmin(), slicedUA, slicedCR, slicedCA,
                unsliced.getSpec());
    }

}
