package mohawk.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mohawk.MohawkInstance;
import mohawk.global.results.ExecutionResult;
import mohawk.singleton.MohawkSettings;

/** Run these JUnit tests before adding any branch to master
 * 
 * @author Jonathan Shahen */

public class MohawkRegressionTests {
    private String defaultParameters = "-loglevel quiet -logfile n";

    @Test
    public void runPositive1() {
        String param = "-input data/regressiontests/positive1.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runPositive2() {
        String param = "-input data/regressiontests/positive2.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runPositive3() {
        String param = "-input data/regressiontests/positive3.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runPositive4() {
        String param = "-input data/regressiontests/positive4.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runHeuristic1() {
        String param = "-input data/regressiontests/heuristic1.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_UNREACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runHeuristic2() {
        String param = "-input data/regressiontests/heuristic2.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    @Test
    public void runHeuristic3() {
        String param = "-input data/regressiontests/heuristic3.mohawk -run all -mode bmc";
        ExecutionResult expectedResult = ExecutionResult.GOAL_REACHABLE;

        MohawkInstance mohawk = new MohawkInstance();
        mohawk.run(params(param));

        assertEquals("The result was not correct!", expectedResult, MohawkSettings.getInstance().lastResult._result);
    }

    private String[] params(String param) {
        return (defaultParameters + " " + param).split(" ");
    }
}
